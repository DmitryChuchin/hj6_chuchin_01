document.addEventListener('DOMContentLoaded', function() {
	var header = document.querySelector('.main-header');
	document.addEventListener('scroll', function() {
		var headerTop = document.body.scrollTop;
		(headerTop > 50) ? header.classList.add('sticked') : header.classList.remove('sticked');
	})
})